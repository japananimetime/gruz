<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any otherUser.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can view the otherUser.
     *
     * @param  App\User  $user
     * @param  App\User  $otherUser
     * @return bool
     */
    public function view(User $user, User $otherUser)
    {
        return false;
    }

    /**
     * Determine whether the user can create a otherUser.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function create(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can update the otherUser.
     *
     * @param  App\User  $user
     * @param  App\User  $otherUser
     * @return bool
     */
    public function update(User $user, User $otherUser)
    {
        return false;
    }

    /**
     * Determine whether the user can delete the otherUser.
     *
     * @param  App\User  $user
     * @param  App\User  $otherUser
     * @return bool
     */
    public function delete(User $user, User $otherUser)
    {
        return false;
    }

    /**
     * Determine whether the user can restore the otherUser.
     *
     * @param  App\User  $user
     * @param  App\User  $otherUser
     * @return bool
     */
    public function restore(User $user, User $otherUser)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the otherUser.
     *
     * @param  App\User  $user
     * @param  App\User  $otherUser
     * @return bool
     */
    public function forceDelete(User $user, User $otherUser)
    {
        return false;
    }
}
