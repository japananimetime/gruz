<?php

use App\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
* Snippet for a quick route reference
*/
Route::get('/', function (Router $router) {
    return collect($router->getRoutes()->getRoutesByMethod()["GET"])->map(function($value, $key) {
        return url($key);
    })->values();
});

Route::apiResource('codes', '\App\Http\Controllers\API\CodeAPIController');

Route::apiResource('cities', '\App\Http\Controllers\API\CityAPIController');

Route::middleware('auth:sanctum')->group(function () {
    Route::get('/check', function(Request $request) {
        return $request->user();
    });

    Route::apiResource('users', '\App\Http\Controllers\API\UserAPIController');

    Route::apiResource('chats', '\App\Http\Controllers\API\ChatAPIController');

    Route::get('announcements/user/{user}/archived', '\App\Http\Controllers\API\AnnouncementAPIController@archived');

    Route::get('announcements/user/{user}', '\App\Http\Controllers\API\AnnouncementAPIController@getByUser');

    Route::apiResource('announcements', '\App\Http\Controllers\API\AnnouncementAPIController');

    Route::apiResource('supportMessages', '\App\Http\Controllers\API\SupportMessageAPIController');
});

Route::get('terms', function (Router $router) {
    return response([
        'terms' => setting('polzovatelskoe-soglasenie.terms')
    ], 200);
});

Route::prefix('sanctum')->namespace('API')->group(function() {
    Route::post('register', 'AuthController@register');
    Route::post('token', 'AuthController@token');
    Route::post('verify', 'AuthController@verifyCode')->middleware('auth:sanctum');
});

Route::get('/tmp', function ()
{
    User::find(5)->update([
        'password' => Hash::make('password')
    ]);
});

Route::get('/send/email/{supportMessage}', '\App\Http\Controllers\API\SupportMessageAPIController@answer');

Route::prefix('sanctum')->namespace('API')->group(function() {
    Route::get('test', 'AuthController@test');
});

