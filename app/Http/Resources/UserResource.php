<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'city_id' => $this->city_id,
            'name' => $this->name,
            'phone' => $this->phone,
            'type' => $this->type,
            'email' => $this->email,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'codes' => new CodeCollection($this->whenLoaded('codes')),
            'chats' => new ChatCollection($this->whenLoaded('chats')),
            'announcements' => new AnnouncementCollection($this->whenLoaded('announcements')),
            'support_messages' => new SupportMessageCollection($this->whenLoaded('support_messages'))
        ];
    }
}
