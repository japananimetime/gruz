<?php

namespace App\Http\Controllers\API;

use App\Chat;
use App\Http\Resources\ChatCollection;
use App\Http\Resources\ChatResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ChatAPIController extends Controller
{
    public function index()
    {
        return new ChatCollection(Chat::paginate());
    }
 
    public function show(Chat $chat)
    {
        return new ChatResource($chat->load(['supportMessages', 'user']));
    }

    public function store(Request $request)
    {
        return new ChatResource(Chat::create($request->all()));
    }

    public function update(Request $request, Chat $chat)
    {
        $chat->update($request->all());

        return new ChatResource($chat);
    }

    public function destroy(Request $request, Chat $chat)
    {
        $chat->delete();

        return response()->noContent();
    }
}
