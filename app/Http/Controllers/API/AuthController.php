<?php

namespace App\Http\Controllers\API;

use App\Code;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\CodeGenerator;
use App\Traits\SendSMS;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    use CodeGenerator, SendSMS;

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'        => ['required', 'string', 'max:255'],
            'phone'       => ['required', 'string', 'max:255'],
            'city_id'     => ['required', 'integer'],
            'password'    => ['required', 'confirmed', 'string', 'min:8'],
            'type'        => ['required', 'integer'],
            'device_name' => ['required', 'string'],
        ]);

        if (!empty(
        User::query()
            ->where('phone', $request->get('phone'))
            ->first()
        )) {
            return response()->json(['error' => $validator->errors()], 403);
        }

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $user = User::onlyTrashed()
                    ->where('phone', $request->get('phone'))
                    ->first()
        ;

        $input              = $request->all();
        $input['password']  = bcrypt($input['password']);
        $input['fcm_token'] = $request->device_name;

        if ($user) {
            $user->restore();
            $user->update($input);
        } else {
            $user = User::create($input);
        }

        // $token = $user->createToken($request->device_name)->plainTextToken;

        $this->sendCode($this->generate($user));

        return response()->json(
            [
                'token' => $user->createToken($request->device_name)->plainTextToken,
                'id'    => $user->id,
                'name'  => $user->name,
            ]
        );
    }

    public function token(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone'       => ['required', 'string', 'max:255'],
            'password'    => ['string', 'min:8'],
            'device_name' => ['required', 'string'],
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $user = User::query()
                    ->where('phone', $request->phone)
                    ->first()
        ;

        if (!$user) {
            return response()->json(['error' => 'The provided credentials are incorrect.'], 401);
        }
        if ($user->type === 0) {
            $this->sendCode($this->generate($user));

            return response()->json(
                [
                    'token' => $user->createToken($request->device_name)->plainTextToken,
                    'id'    => $user->id,
                    'name'  => $user->name,
                ]
            );
        }

        if (!Hash::check($request->password, $user->password)) {
            return response()->json(['error' => 'The provided credentials are incorrect.'], 401);
        }

        $user->update(
            [
                'fcm_token' => $request->device_name,
            ]
        );

        return response()->json(
            [
                'token' => $user->createToken($request->device_name)->plainTextToken,
                'id'    => $user->id,
                'name'  => $user->name,
            ]
        );
    }

    public function verifyCode(Request $request)
    {
        $code = Code
            ::query()
            ->where('user_id', Auth::user()->id)
            ->where('is_valid', 't')
            ->latest('created_at')
            ->first()
        ;
        Log::debug('$code', [
            'code' => $code,
        ]);

        if ($code) {
            if ($code->code == $request->code) {
                return response([], 200);
            }
        }

        return response([], 403);
    }

    public function test()
    {
        $api = new \Mobizon\MobizonApi(env('MOBIZONE_KEY'), 'api.mobizon.kz');

        echo 'Fetch all user alphanames...' . PHP_EOL;
        if ($api->call('alphaname', 'list')) {
            if ($api->hasData() && $api->hasData('items')) {
                $data = $api->getData('items');
                echo 'Total of ' . $api->getData('totalItemCount') . ' items found. Current subset of data:' . PHP_EOL;
                foreach ($data as $row) {
                    echo str_pad($row->alphaname->name, 30)
                        . "\t" . $row->alphanameId
                        . "\t" . $row->globalStatus
                        . "\t" . $row->partnerStatus
                        . "\t" . $row->description . PHP_EOL;
                }
            }
        } else {
            echo 'No alphanames found.' . PHP_EOL;
        }
    }
}
