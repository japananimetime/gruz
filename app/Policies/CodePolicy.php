<?php

namespace App\Policies;

use App\User;
use App\Code;
use Illuminate\Auth\Access\HandlesAuthorization;

class CodePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any code.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can view the code.
     *
     * @param  App\User  $user
     * @param  App\Code  $code
     * @return bool
     */
    public function view(User $user, Code $code)
    {
        return false;
    }

    /**
     * Determine whether the user can create a code.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function create(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can update the code.
     *
     * @param  App\User  $user
     * @param  App\Code  $code
     * @return bool
     */
    public function update(User $user, Code $code)
    {
        return false;
    }

    /**
     * Determine whether the user can delete the code.
     *
     * @param  App\User  $user
     * @param  App\Code  $code
     * @return bool
     */
    public function delete(User $user, Code $code)
    {
        return false;
    }

    /**
     * Determine whether the user can restore the code.
     *
     * @param  App\User  $user
     * @param  App\Code  $code
     * @return bool
     */
    public function restore(User $user, Code $code)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the code.
     *
     * @param  App\User  $user
     * @param  App\Code  $code
     * @return bool
     */
    public function forceDelete(User $user, Code $code)
    {
        return false;
    }
}
