<?php

namespace App\Http\Controllers\API;

use App\Code;
use App\Http\Resources\CodeCollection;
use App\Http\Resources\CodeResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CodeAPIController extends Controller
{
    public function index()
    {
        return new CodeCollection(Code::paginate());
    }
 
    public function show(Code $code)
    {
        return new CodeResource($code->load(['user']));
    }

    public function store(Request $request)
    {
        return new CodeResource(Code::create($request->all()));
    }

    public function update(Request $request, Code $code)
    {
        $code->update($request->all());

        return new CodeResource($code);
    }

    public function destroy(Request $request, Code $code)
    {
        $code->delete();

        return response()->noContent();
    }
}
