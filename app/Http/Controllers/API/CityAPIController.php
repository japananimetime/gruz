<?php

namespace App\Http\Controllers\API;

use App\City;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\CityCollection;
use App\Http\Resources\CityResource;

class CityAPIController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new CityCollection(City::orderBy('name', 'asc')->paginate());
    }

    public function show(City $city)
    {
        return new CityResource($city);
    }

    public function store(Request $request)
    {
        return new CityResource(City::create($request->all()));
    }

    public function update(Request $request, City $city)
    {
        $city->update($request->all());

        return new CityResource($city);
    }

    public function destroy(Request $request, City $city)
    {
        $city->delete();

        return response()->noContent();
    }
}
