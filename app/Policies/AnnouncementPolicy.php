<?php

namespace App\Policies;

use App\User;
use App\Announcement;
use Illuminate\Auth\Access\HandlesAuthorization;

class AnnouncementPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any announcement.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can view the announcement.
     *
     * @param  App\User  $user
     * @param  App\Announcement  $announcement
     * @return bool
     */
    public function view(User $user, Announcement $announcement)
    {
        return false;
    }

    /**
     * Determine whether the user can create a announcement.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function create(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can update the announcement.
     *
     * @param  App\User  $user
     * @param  App\Announcement  $announcement
     * @return bool
     */
    public function update(User $user, Announcement $announcement)
    {
        return false;
    }

    /**
     * Determine whether the user can delete the announcement.
     *
     * @param  App\User  $user
     * @param  App\Announcement  $announcement
     * @return bool
     */
    public function delete(User $user, Announcement $announcement)
    {
        return false;
    }

    /**
     * Determine whether the user can restore the announcement.
     *
     * @param  App\User  $user
     * @param  App\Announcement  $announcement
     * @return bool
     */
    public function restore(User $user, Announcement $announcement)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the announcement.
     *
     * @param  App\User  $user
     * @param  App\Announcement  $announcement
     * @return bool
     */
    public function forceDelete(User $user, Announcement $announcement)
    {
        return false;
    }
}
