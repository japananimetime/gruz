<?php

namespace App\Traits;

use Carbon\Carbon;
use Mobizon\MobizonApi;

trait SendSMS
{
    public function sendCode($data){

        $api = new MobizonApi(env('MOBIZONE_KEY'), 'api.mobizon.kz');
//        $alphaname = 'SousChef';
        if ($api->call('message',
            'sendSMSMessage',
            array(
                'recipient' => $data['phone'],
                'text' => "Городские Грузоперевозки\n" . 'Ваш код: ' . $data['code']. "\nИстекает " . Carbon::parse($data['expire_at'])->toDateTimeString(),
                'from' => 'GorodskieGr',
                //Optional, if you don't have registered alphaname, just skip this param and your message will be sent with our free common alphaname.
            ))
        ) {
            $messageId = $api->getData('messageId');


            if ($messageId) {
                $messageStatuses = $api->call(
                    'message',
                    'getSMSStatus',
                    array(
                        'ids' => array($messageId, '13394', '11345', '4393')
                    ),
                    array(),
                    true
                );

                if ($api->hasData()) {
                    $results = [];
                    foreach ($api->getData() as $messageInfo) {
                        $results[] = 'Message # ' . $messageInfo->id . " status: " . $messageInfo->status;
                    }
                }
            }
            return 1;
        } else {
            return 0;
        }

    }
}
