<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class banCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::guard('api')->check()){
            if(!is_null(Auth::guard('api')->user()->deleted_at)){
                foreach(Auth::guard('api')->user()->tokens as $token) {
                    $token->revoke();
                }
            }
        }

        return $next($request);
    }
}
