<?php

namespace App\Http\Controllers\API;

use App\SupportMessage;
use App\Http\Resources\SupportMessageCollection;
use App\Http\Resources\SupportMessageResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\Answer;
use Illuminate\Support\Facades\Mail;

class SupportMessageAPIController extends Controller
{
    public function index()
    {
        return new SupportMessageCollection(SupportMessage::paginate());
    }

    public function show(SupportMessage $supportMessage)
    {
        return new SupportMessageResource($supportMessage->load(['user', 'chat']));
    }

    public function store(Request $request)
    {
        return new SupportMessageResource(SupportMessage::create($request->all()));
    }

    public function update(Request $request, SupportMessage $supportMessage)
    {
        $supportMessage->update($request->all());

        return new SupportMessageResource($supportMessage);
    }

    public function destroy(Request $request, SupportMessage $supportMessage)
    {
        $supportMessage->delete();

        return response()->noContent();
    }

    public function answer(Request $request, SupportMessage $supportMessage)
    {
        Mail::to($supportMessage->email)->send(new Answer($request->text));
        return response([], 200);
    }
}
