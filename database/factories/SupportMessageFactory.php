<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Chat;
use App\SupportMessage;
use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(SupportMessage::class, function (Faker $faker) {
    return [
        'chat_id' => Chat::inRandomOrder()->first()->id,
        'user_id' => User::inRandomOrder()->first()->id,
        'text' => $faker->word()
    ];
});
