<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\City;
use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'city_id' => City::inRandomOrder()->first()->id,
        'name' => $faker->name(),
        'phone' => $faker->phoneNumber,
        'type' => random_int(0, 1),
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => $faker->date(),
        'password' => Hash::make('secretsecret')
    ];
});
