<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Announcement;
use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Announcement::class, function (Faker $faker) {
    return [
        'user_id' => User::inRandomOrder()->first()->id,
        'body' => $faker->text('512'),
        'phone' => $faker->phoneNumber,
        'from' => $faker->address,
        'to' => $faker->address,
        'status' => random_int(0, 2)
    ];
});
