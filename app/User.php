<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Sanctum\HasApiTokens;
use \Illuminate\Database\Eloquent\Collection;

/**
 * Class User
 *
 * @package App
 * @property-read  Collection|\App\Code[] codes
 * @property-read  Collection|\App\Chat[] chats
 * @property-read  Collection|\App\Announcement[] announcements
 * @property-read  Collection|\App\SupportMessage[] supportMessages
 */
class User extends \TCG\Voyager\Models\User
{
    use HasApiTokens, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'city_id', 'name', 'phone', 'type', 'email', 'password', 'fcm_token', 'deleted_at'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        //
    ];

    /**
     * Get the Codes for the User.
     */
    public function codes()
    {
        return $this->hasMany(\App\Code::class);
    }


    /**
     * Get the Chats for the User.
     */
    public function chats()
    {
        return $this->hasMany(\App\Chat::class);
    }


    /**
     * Get the Announcements for the User.
     */
    public function announcements()
    {
        return $this->hasMany(\App\Announcement::class);
    }


    /**
     * Get the SupportMessages for the User.
     */
    public function supportMessages()
    {
        return $this->hasMany(\App\SupportMessage::class);
    }

    public function getTypeBrowseAttribute()
    {
        if($this['type'] == 1){
            return 'Водитель';
        }
        if($this['type'] == 0){
            return 'Клиент';
        }
        return $this['type'];
    }
}
