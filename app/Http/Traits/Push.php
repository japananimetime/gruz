<?php

namespace App\Http\Traits;

trait Push
{
    public function sendPush($title, $text, $token, $data = null)
    {
        $url = 'https://fcm.googleapis.com/fcm/send';
        $YOUR_API_KEY = 'AAAA9dPe4tw:APA91bFAhLk4R4wug_tRff3t9MBjurhDSpsDFgjwAulLPa_2DWIYVl5khT8oGDEloKmnKta05nMxgLoysX9yfwq-15G1KvH6GJehGwugU3jMD4jApleaC1J-bEuxP9AwYqNZmx29TJQ7'; // Server key
        $request_body = array(
            'to' => $token,
            'notification' => array(
                'title' => $title,
                'body' => $text,
                'icon' => 'http://storage.en.iprize.co/icFire.png',
                'badge' => 1,
                "sound" => 'default'
                // 'click_action' => asset('/'),
            ),
            "priority" => "high",
            "foreground" =>  false,
            "userInteraction" =>  false,
            "content_available" =>  true,
            "data" => $data
        );
        $fields = json_encode($request_body);
        $request_headers = array(
            'Content-Type: application/json',
            'Authorization: key=' . $YOUR_API_KEY,
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $response = curl_exec($ch);
        curl_close($ch);
    }
}
