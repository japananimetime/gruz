<?php

namespace App\Traits;

use App\Code;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

trait CodeGenerator
{
    /**
     * @throws \Exception
     */
    public function generate($user){

        $code = random_int(0, 9) . random_int(0, 9) . random_int(0, 9) . random_int(0, 9);


        while (!is_null(Code::where('code', $code)
            ->where('is_valid', 1)
            ->first()))
        {
            $code = random_int(0, 9) . random_int(0, 9) . random_int(0, 9) . random_int(0, 9);
        }


        if($user->phone === '77073163307'){
            $code = '1111';
        }

        $created = Code::create([
            'code' => $code,
            'user_id' => $user->id,
            'is_valid' => 1
        ]);

        return [
            'code' => $code,
            'expire_at' => $created->created_at->addHours(6)->toDateTimeString(),
            'phone' => $user->phone
        ];
    }
}
