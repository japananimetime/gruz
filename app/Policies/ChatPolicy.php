<?php

namespace App\Policies;

use App\User;
use App\Chat;
use Illuminate\Auth\Access\HandlesAuthorization;

class ChatPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any chat.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can view the chat.
     *
     * @param  App\User  $user
     * @param  App\Chat  $chat
     * @return bool
     */
    public function view(User $user, Chat $chat)
    {
        return false;
    }

    /**
     * Determine whether the user can create a chat.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function create(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can update the chat.
     *
     * @param  App\User  $user
     * @param  App\Chat  $chat
     * @return bool
     */
    public function update(User $user, Chat $chat)
    {
        return false;
    }

    /**
     * Determine whether the user can delete the chat.
     *
     * @param  App\User  $user
     * @param  App\Chat  $chat
     * @return bool
     */
    public function delete(User $user, Chat $chat)
    {
        return false;
    }

    /**
     * Determine whether the user can restore the chat.
     *
     * @param  App\User  $user
     * @param  App\Chat  $chat
     * @return bool
     */
    public function restore(User $user, Chat $chat)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the chat.
     *
     * @param  App\User  $user
     * @param  App\Chat  $chat
     * @return bool
     */
    public function forceDelete(User $user, Chat $chat)
    {
        return false;
    }
}
