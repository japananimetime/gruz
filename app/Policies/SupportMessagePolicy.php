<?php

namespace App\Policies;

use App\User;
use App\SupportMessage;
use Illuminate\Auth\Access\HandlesAuthorization;

class SupportMessagePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any supportMessage.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can view the supportMessage.
     *
     * @param  App\User  $user
     * @param  App\SupportMessage  $supportMessage
     * @return bool
     */
    public function view(User $user, SupportMessage $supportMessage)
    {
        return false;
    }

    /**
     * Determine whether the user can create a supportMessage.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function create(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can update the supportMessage.
     *
     * @param  App\User  $user
     * @param  App\SupportMessage  $supportMessage
     * @return bool
     */
    public function update(User $user, SupportMessage $supportMessage)
    {
        return false;
    }

    /**
     * Determine whether the user can delete the supportMessage.
     *
     * @param  App\User  $user
     * @param  App\SupportMessage  $supportMessage
     * @return bool
     */
    public function delete(User $user, SupportMessage $supportMessage)
    {
        return false;
    }

    /**
     * Determine whether the user can restore the supportMessage.
     *
     * @param  App\User  $user
     * @param  App\SupportMessage  $supportMessage
     * @return bool
     */
    public function restore(User $user, SupportMessage $supportMessage)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the supportMessage.
     *
     * @param  App\User  $user
     * @param  App\SupportMessage  $supportMessage
     * @return bool
     */
    public function forceDelete(User $user, SupportMessage $supportMessage)
    {
        return false;
    }
}
