<?php

namespace App\Http\Controllers\API;

use App\Announcement;
use App\Http\Resources\AnnouncementCollection;
use App\Http\Resources\AnnouncementResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Traits\Push;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class AnnouncementAPIController extends Controller
{
    use Push;

    public function index(Request $request)
    {
        $fields = [
            'announcements.id',
            "announcements.user_id",
            "announcements.body",
            "announcements.from",
            "announcements.to",
            "announcements.status",
            "announcements.created_at"
        ];

        if($request->user()->payed){
            $fields[] = "phone";
        }

        $var = Announcement::with('user')
            ->where('created_at', '>', Carbon::now()->subHours(8));
            if(isset($request->city_id)){
                $var = $var->whereHas('user', function($q) use ($request)
                {
                    $q->where('city_id', $request->city_id);
                });
            }
        return new AnnouncementCollection(
                // ->select(...$fields)
                $var
                    ->orderBy('announcements.created_at', 'desc')
                    ->paginate());
    }

    public function show(Announcement $announcement)
    {
        if($announcement->created_at >= Carbon::now()->subHours(8)){
            return new AnnouncementResource($announcement->load(['user']));
        }
        return response()->noContent();
    }

    public function store(Request $request)
    {
        $text = '';
        $i = 0;
        foreach(explode(' ', $request->body) as $word) {
            if($i < 4){
                $text = $text . ' ' . $word;
                $i = $i + 1;
            }
        }

        $announcement = new AnnouncementResource(Announcement::create($request->all()));

        $this->sendPush('Новый заказ', Carbon::now()->toDateTimeString() . "\n" . $text, '/topics/gruzz' . $announcement->user->city_id, ['announcement_id' => $announcement->id]);

        return $announcement;
    }

    public function update(Request $request, Announcement $announcement)
    {
        // if(Auth::user()->id == $announcement->user_id){
            $announcement->update($request->all());
        // }

        return new AnnouncementResource($announcement);
    }

    public function destroy(Request $request, Announcement $announcement)
    {
        if(Auth::user()->id == $announcement->user_id){
            $announcement->update([
                'created_at' => Carbon::now()->subHours(8)
            ]);
        }

        return response()->noContent();
    }

    public function create()
    {
        # code...
    }

    public function edit()
    {
        # code...
    }

    public function archived(Request $request, User $user)
    {
        return new AnnouncementCollection(Announcement::with('user')
            ->where('created_at', '<', Carbon::now()->subHours(8))
            ->where('created_at', '>', Carbon::now()->subHours(48))
            ->where('user_id', $user->id)
            ->paginate());
    }

    public function getByUser(Request $request, User $user)
    {
        return new AnnouncementCollection(Announcement::with('user')
            ->where('created_at', '>', Carbon::now()->subHours(8))
            ->orderBy('created_at', 'desc')
            ->where('user_id', $user->id)
            ->paginate());
    }
}
