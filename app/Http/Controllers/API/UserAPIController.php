<?php

namespace App\Http\Controllers\API;

use App\User;
use App\Http\Resources\UserCollection;
use App\Http\Resources\UserResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserAPIController extends Controller
{
    public function index()
    {
        return new UserCollection(User::paginate());
    }

    public function show(User $user)
    {
        return new UserResource($user->load(['codes', 'chats', 'announcements', 'supportMessages']));
    }

    public function store(Request $request)
    {
        return new UserResource(User::create($request->all()));
    }

    public function update(Request $request, User $user)
    {
        $user->update($request->all());

        return new UserResource($user);
    }

    public function destroy(Request $request, User $user)
    {
        $user->announcements()->delete();
        $user->chats()->delete();
        $user->codes()->delete();
        $user->delete();

        return response()->noContent();
    }
}
